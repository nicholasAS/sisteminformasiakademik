/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisteminformasiakademik;

import java.util.LinkedList;

/**
 *
 * @author ASUS
 */
public class Jadwal {

    public LinkedList<Matkul> daftarJadwal = new LinkedList<>();
    public String namaHari;
    
    public Jadwal(String namaHari){
        this.namaHari = namaHari;
        
    }

    public void printJadwal() {
        System.out.println(" ----- " + this.namaHari + " -----");
        int no = 0;
        for (Matkul i : this.daftarJadwal) {
            System.out.print(no+1);
            System.out.println( i);
            no++;
        }
        System.out.println("\n");

    }

    //fungsi ini untuk sorting jam mulai sehingga ditampilkan berurutan
    //dari kecil ke besar
    public  void addSorted(Matkul mtk) {
        int i = 0;

        if (this.daftarJadwal.isEmpty()) {
            this.daftarJadwal.add(mtk);

        } else {
            while (i < this.daftarJadwal.size() && this.daftarJadwal.get(i).jamMulai < mtk.jamMulai) {
                i++;
            }
            this.daftarJadwal.add(i, mtk);

        }

    }

}
