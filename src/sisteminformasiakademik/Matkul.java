/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisteminformasiakademik;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Matkul {

    public String nama;
    public String kode;
    public int sks;
    public Dosen dosen;
    public String ruangan;
    public Double jamMulai;
    public int durasi;
  
    public ArrayList<Mahasiswa>daftarMhs = new ArrayList<Mahasiswa>(); 
   

    public Matkul(String nama, String kode, int sks, Dosen dosen,String ruangan,Double jamMulai,int durasi) {
        this.nama = nama;
        this.kode = kode;
        this.sks = sks;
        this.dosen = dosen;
        this.ruangan = ruangan;
        this.jamMulai = jamMulai;
        this.durasi = durasi;

    }

    @Override
    public String toString() {
        
        //format number supaya mirip kayak jam
        String jm = String.format("%.2f", this.jamMulai); 
        String ja = String.format("%.2f",this.jamMulai+this.durasi);
        
        return  " " + this.nama + "\n  " + "Dosen : " + this.dosen.nama + "\n  " + this.ruangan + "\n  " + jm+ " - " + ja + "\n  Daftar Mahasiswa : " + printDaftarMahasiswa();
    
    }
    
    public String printDaftarMahasiswa(){
        String temp = "";
        for(Mahasiswa i: daftarMhs){
            temp = i + ", ";
        }
        return temp;
    }

}
