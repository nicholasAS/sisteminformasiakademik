/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisteminformasiakademik;

/**
 *
 *  Nicholas Anthony S 1118049
    Ferani Christy 1118015
    Johanes V 1118019
    Fransisskus R F - 111805

 */
public class SistemInformasiAkademik {

    /**
     * @param args the command line arguments
     */
    //inisialisasi objek mahasiswa 
    public static Mahasiswa MHS1 = new Mahasiswa("Nicholas", "1118049");

    public static Mahasiswa MHS2 = new Mahasiswa("Anthony", "1118050");

    //inisialisasi objek Dosen
    public static final Dosen DOSEN1 = new Dosen("Suhartono", "584949494");
    public static final Dosen DOSEN2 = new Dosen("Little Bambang", "30938474");
    public static final Dosen DOSEN3 = new Dosen("Big Bambang", "123445767");

    //inisialisasi objek matkul
    public static Matkul ALGORITMA = new Matkul("algoritma", "if-102", 4, DOSEN1, "R.202", 8.00, 2);

    public static Matkul WEB_DESAIN = new Matkul("Web Desain", "if-103", 5, DOSEN1, "Lab komputer", 7.30, 3);
    public static Matkul WEB_PROG = new Matkul("Web Programming PHP", "if-104", 5, DOSEN1, "Lab komputer", 15.0, 3);

    public static Matkul KALKULUS = new Matkul("Kalkulus", "if-169", 3, DOSEN2, "B.006", 16.00, 2);
    public static Matkul STRUKDAT = new Matkul("Struktur Data", "if-169", 3, DOSEN2, "R.207", 17.00, 2);
    public static Matkul PBO = new Matkul("Pemrograman Berorientasi Objek", "if-123", 3, DOSEN2, "R.207", 20.00, 2);

    public static Matkul AI = new Matkul("Kecerdasan Buatan", "if-321", 5, DOSEN3, "B.003", 12.00, 2);
    public static Matkul NLP = new Matkul("Neuro Language Processing", "if-777", 2, DOSEN3, "R.201", 13.00, 2);
    public static Matkul KRIPTO = new Matkul("Kriptografi", "if-696", 4, DOSEN3, "R.401", 18.00, 2);

    public static void main(String[] args) {
        // TODO code application logic here

        ALGORITMA.daftarMhs.add(MHS1);
        ALGORITMA.daftarMhs.add(MHS2);

        WEB_DESAIN.daftarMhs.add(MHS1);

        WEB_PROG.daftarMhs.add(MHS1);

        KALKULUS.daftarMhs.add(MHS1);
        KALKULUS.daftarMhs.add(MHS2);

        STRUKDAT.daftarMhs.add(MHS2);

        PBO.daftarMhs.add(MHS2);

        AI.daftarMhs.add(MHS1);
        AI.daftarMhs.add(MHS2);

        NLP.daftarMhs.add(MHS2);

        KRIPTO.daftarMhs.add(MHS2);

        Jadwal senin = new Jadwal("Senin");
        Jadwal selasa = new Jadwal("Selasa");
        Jadwal rabu = new Jadwal("Rabu");
        Jadwal kamis = new Jadwal("Kamis");
        Jadwal jumat = new Jadwal("Jumat");

        senin.addSorted(STRUKDAT);
        senin.addSorted(PBO);
        senin.addSorted(ALGORITMA);

        selasa.addSorted(WEB_DESAIN);
        selasa.addSorted(WEB_PROG);

        rabu.addSorted(WEB_PROG);
        rabu.addSorted(AI);

        kamis.addSorted(KALKULUS);
        kamis.addSorted(NLP);

        jumat.addSorted(PBO);
        jumat.addSorted(KRIPTO);

        senin.printJadwal();
        selasa.printJadwal();
        rabu.printJadwal();
        kamis.printJadwal();
        jumat.printJadwal();

    }

}
